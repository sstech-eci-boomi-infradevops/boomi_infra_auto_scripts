variable "region" {
  default = "us-east-1"
}

variable "cluster_name" {
  default = "ECI_BOOMI_PROD_PAC_EKS"
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::327792311537:group/Admins"
      username = "{{SessionName}}"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::327792311537:user/Rajesh.Reddy@sstech.us"
      username = "Rajesh.Reddy@sstech.us"
      groups   = ["system:masters"]
    },
    {
      userarn  = "arn:aws:iam::327792311537:user/srikanth.lanka@sstech.us"
      username = "srikanth.lanka@sstech.us"
      groups   = ["system:masters"]
    },
  ]
}
