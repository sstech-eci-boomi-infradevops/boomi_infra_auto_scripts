locals{
  # EKS Cluster Variables
  cluster_name = "ECI_BOOMI_PROD_PAC_EKS"
  vpc_id = "vpc-06f1f8d4204ebd8ac"
  region = "us-east-1"
  subnets = ["subnet-041061b9f303a1974", "subnet-07bd81329b241c28d", "subnet-0a4ca492b97e37e31"]
  cluster_version = "1.19"
  environment = "PROD"
  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  # Managed nodeGroups Variables
  desired_capacity = 3
  min_capacity = 3
  max_capacity = 12
  instance_types = ["m5.xlarge"]
  capacity_type = "ON_DEMAND"
  ssh_key_name = "ECI_BOOMI_PROD"
  ami_type = "AL2_x86_64"
  disk_size = 50
  disk_type = "gp3"
}
