resource "aws_lb" "eci_boomi_prod_gateway_lb" {
  name               = "ECI-BOOMI-PROD-GATEWAY-LB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.eci_boomi_prod_gateway_sg.id}"]
  subnets            = local.subnets

  enable_deletion_protection = true

  # access_logs {
  #   bucket  = aws_s3_bucket.lb_logs.bucket
  #   prefix  = "test-lb"
  #   enabled = true
  # }

  tags = {
    Environment = "PROD"
  }
}

resource "aws_lb_listener" "eci_boomi_prod_gateway_lb_listener_https" {
  load_balancer_arn = aws_lb.eci_boomi_prod_gateway_lb.arn
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = "${var.ssl_arn}"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.eci_boomi_prod_gateway_tg_8077.arn
  }
}

resource "aws_lb_listener" "eci_boomi_prod_gateway_lb_listener_http" {
  load_balancer_arn = aws_lb.eci_boomi_prod_gateway_lb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = "443"
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

resource "aws_lb_target_group" "eci_boomi_prod_gateway_tg_8077" {
  name        = "ECI-BOOMI-PROD-GATEWAY-TG-8077"
  port        = 8077
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  health_check {
    interval            = 30
    path                = "/_admin/status"
    port                = 8077
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    protocol            = "HTTP"
    matcher             = "200,202"
  }
}

resource "aws_lb_target_group" "eci_boomi_prod_gateway_tg_18077" {
  name        = "ECI-BOOMI-PROD-GATEWAY-TG-18077"
  port        = 18077
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  health_check {
    interval            = 30
    path                = "/_admin/status"
    port                = 8077
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 5
    protocol            = "HTTP"
    matcher             = "200,202"
  }
}

resource "aws_lb_listener_rule" "oauth" {
  listener_arn = aws_lb_listener.eci_boomi_prod_gateway_lb_listener_https.arn
  priority     = 1

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.eci_boomi_prod_gateway_tg_8077.arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  condition {
    host_header {
      values = ["live.api.elysiumanalytics.ai"]
    }
  }
}

resource "aws_lb_listener_rule" "developer" {
  listener_arn = aws_lb_listener.eci_boomi_prod_gateway_lb_listener_https.arn
  priority     = 2

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.eci_boomi_prod_gateway_tg_18077.arn
  }

  condition {
    path_pattern {
      values = ["/*"]
    }
  }

  condition {
    host_header {
      values = ["developer.api.elysiumanalytics.ai"]
    }
  }
}

# resource "aws_route53_record" "oauth" {
#   zone_id = "${var.dns_zone_id}"
#   name    = "oauth.api.elysiumanalytics.ai"
#   type    = "A"
#
#   alias {
#     name                   = aws_elb.eci_boomi_prod_gateway_lb.dns_name
#     zone_id                = aws_elb.eci_boomi_prod_gateway_lb.zone_id
#     evaluate_target_health = true
#   }
# }
