provider "aws" {
  region  = "us-east-1"
}

terraform {
  backend "s3" {
      bucket = "sstech-prod-terraform"
      key = "gateway/asg/terraform.tfstate"
      region = "us-east-1"
    }
}

resource "aws_launch_configuration" "eci_boomi_prod_gateway_lc" {
  name_prefix          = "${var.service_name}_LC_"
  image_id        = "${var.ami}"
  instance_type   = "${var.instance_type}"
  security_groups = ["${aws_security_group.eci_boomi_prod_gateway_sg.id}"]
  key_name        = "${var.ssh_key_name}"

    user_data = <<-EOF
    #!/bin/bash
    apt-get update
    apt-get -y install nfs-common
    mkdir /mnt/boomi
    mkdir -p /opt/boomi/local
    sh -c 'echo "fs-798263ce.efs.us-east-1.amazonaws.com:/ /mnt/boomi nfs4 defaults,vers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab'
    mount -a
    useradd -u 1001 boomi
    chown -R boomi:boomi /mnt/boomi/Gateway_ECI_BOOMI_PROD_GATEWAY /opt/boomi/local
    su boomi -c "/mnt/boomi/Gateway_ECI_BOOMI_PROD_GATEWAY/bin/atom start"
    EOF

  lifecycle {
    create_before_destroy = true
  }

  root_block_device {
    volume_type = "gp2"
    volume_size = "50"
  }

}

resource "aws_security_group" "eci_boomi_prod_gateway_sg" {
  name   = "ECI_BOOMI_PROD_GATEWAY_SG"
  vpc_id = "${var.vpc_id}"
}

resource "aws_security_group_rule" "inbound_http" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_https" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 443
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_gateway_tcp1" {
  from_port         = 8077
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 8077
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_gateway_tcp2" {
  from_port         = 18077
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 18077
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "inbound_gateway_unicast" {
  from_port         = 7800
  protocol          = "tcp"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 7800
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}


resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.eci_boomi_prod_gateway_sg.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

# data "aws_availability_zone" "available" {}

resource "aws_autoscaling_group" "eci_boomi_prod_gateway_asg" {
  name                 = "${var.service_name}_ASG"
  launch_configuration = "${aws_launch_configuration.eci_boomi_prod_gateway_lc.name}"
  vpc_zone_identifier  = ["subnet-062f9c1ed0ba8f88d","subnet-029709e5042465fe2","subnet-07ee4168054761d6c"]
  # availability_zones = ["us-east-1a","us-east-1b","us-east-1c"]

  # target_group_arns = ["${var.target_group_arn}"]
  max_size                  = 9
  min_size                  = 3
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 3
  force_delete              = true
  target_group_arns         = ["${aws_lb_target_group.eci_boomi_prod_gateway_tg_8077.arn}","${aws_lb_target_group.eci_boomi_prod_gateway_tg_18077.arn}"]

  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }

  tag {
    key                 = "Name"
    value               = "${var.service_name}"
    propagate_at_launch = true
  }
}
