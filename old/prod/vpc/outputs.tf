output "vpc_id" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.vpc_id
}

output "public_subnet_ids" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.public_subnets
}

output "private_subnet_ids" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.private_subnets
}
