provider "aws" {
  region  = "us-east-1"
}

locals{
  # EKS Cluster Variables
  eks_cluster_name = "ECI_BOOMI_PROD_PAC_EKS"
}

terraform {
  backend "s3" {
    bucket = "sstech-prod-terraform"
    key = "pac/vpc/terraform.tfstate"
    region = "us-east-1"
  }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "ECI_BOOMI_PROD_PAC_VPC"
  cidr = "172.20.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["172.20.1.0/24", "172.20.2.0/24", "172.20.3.0/24"]
  public_subnets  = ["172.20.101.0/24", "172.20.102.0/24", "172.20.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway  = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }


  tags = {
    Terraform = "true"
    Environment = "PROD"
  }
}
