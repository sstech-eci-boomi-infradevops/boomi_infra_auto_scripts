variable "aws_region" {
  default = "us-east-1"
}

variable "bation_key_pair"{
  default = "ECI_BOOMI_PROD"
}

variable "instance_type" {
  type        = string
  default     = "t2.medium"
  description = "Bastion instance type"
}

variable "user_data" {
  type        = list(string)
  default     = []
  description = "User data content"
}

variable "associate_public_ip_address" {
  type        = bool
  default     = true
  description = "Whether to associate public IP to the instance."
}

variable "dns_zone_id" {
  default = "Z02531311E88FM4AWXV2P"
}
