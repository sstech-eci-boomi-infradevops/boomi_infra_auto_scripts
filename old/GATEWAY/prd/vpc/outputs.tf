output "vpc_id" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.vpc_id
}

output "public_subnet_ids" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.public_subnets
}

output "private_subnet_ids" {
  description = "Endpoint for EKS control plane."
  value       = module.vpc.private_subnets
}

output "bastion_sg_id" {
  value = module.bastion.security_group_id
}

output "efs_id" {
  value       = module.efs.id
  description = "EFS ID"
}

output "efs_host" {
  value       = module.efs.host
  description = "Route53 DNS hostname for the EFS"
}

output "efs_dns_name" {
  value       = module.efs.dns_name
  description = "EFS DNS name"
}

output "efs_mount_target_dns_names" {
  value       = module.efs.mount_target_dns_names
  description = "List of EFS mount target DNS names"
}

output "efs_mount_target_ids" {
  value       = module.efs.mount_target_ids
  description = "List of EFS mount target IDs (one per Availability Zone)"
}

output "efs_mount_target_ips" {
  value       = module.efs.mount_target_ips
  description = "List of EFS mount target IPs (one per Availability Zone)"
}
