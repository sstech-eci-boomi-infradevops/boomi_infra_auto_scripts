provider "aws" {
  region  = "us-east-1"
}

terraform {
  backend "s3" {
      bucket = "sstech-prod-terraform"
      key = "gateway/vpc/terraform.tfstate"
      region = "us-east-1"
    }
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "ECI_BOOMI_PROD_GATEWAY_VPC"
  cidr = "172.21.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["172.21.1.0/24", "172.21.2.0/24", "172.21.3.0/24"]
  public_subnets  = ["172.21.101.0/24", "172.21.102.0/24", "172.21.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway  = true

  tags = {
    Terraform = "true"
    Environment = "PROD"
  }
}
