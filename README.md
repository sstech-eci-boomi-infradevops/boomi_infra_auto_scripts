# ECI Boomi Infrastructure Setup

This repository contains Terraform templates for setting up of the complete AWS infrastructure for running ECI Boomi environment/platform which includes **VPC** with 3 private and 3 public subnets, highly available **EKS Cluster** for running PAC, AWS **Autoscaling Group** for running Gateway.

## Architecture Design

![ECI_BOOMI](/eci_boomi_infra/prod/ECI_BOOMI_PAC.png)

## Pre-requesties

 - Install [Terraform](https://www.terraform.io/)
 - Install [kubectl](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html)
 - Install [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
 - Setup [AWS Credentials](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html)

# Deployment Instruction

**Note**: This instruction are going to be used to deploy a production environment which is already deployed.

## Lets get the code
```hcl
git clone https://gitlab.com/sstech-eci-boomi-infradevops/boomi_infra_auto_scripts.git && \
cd eci_boomi_infra/$ENV
```
If **ENV=prod**

**Note**:  You will need to update the variables and properties as per your environment. Please open the templates *.tf files and you will find where to change accordingly.

Once the code has been checked out, the first step is to deploy the VPC, to do so, please follow the below steps:

## Setup VPC

```hcl
cd $ENV/vpc && \
terraform init && terraform apply
 ```
It will ask for your confirmation so type yes or no to continue.

Once the VPC is deployed successfully, you some output which will print the ID's of the VPC and Subnets like below:

    private_subnet_ids = [
      "subnet-xxxxxxxxxxxx",
      "subnet-xxxxxxxxxxxx",
      "subnet-xxxxxxxxxxxx",
    ]
    public_subnet_ids = [
      "subnet-xxxxxxxxxxxx",
      "subnet-xxxxxxxxxxxx",
      "subnet-xxxxxxxxxxxx",
    ]
    vpc_id = "vpc-xxxxxxxxxxxx"

Please make a note of the same as these values are required for deploying the EKS cluster and ASG.

## Setup EKS (for PAC)
Please open and update the variables/values in the below templates

```hcl
$ENV/eks/locals.tf
$ENV/eks/variables.tf
```
```hcl
cd $ENV/eks && \
terraform init && terraform apply
 ```
It will ask for your confirmation so type yes or no to continue.

## Setup AWS Autoscaling Group (for Gateway)
Please open and update the variables/values in the below templates

```hcl
$ENV/eks/locals.tf
$ENV/eks/variables.tf
```
```hcl
cd $ENV/eks && \
terraform init && terraform apply
 ```
It will ask for your confirmation so type yes or no to continue.
