resource "aws_lb" "eci_boomi_gateway_lb" {
  name               = local.load_balancer_name
  internal           = true
  load_balancer_type = local.load_balancer_type
  subnets            = local.private_subnets
  enable_deletion_protection = true
  tags = {
    Environment = local.environment
  }
}

resource "aws_lb_listener" "eci_boomi_gateway_lb_listener_8077" {
  load_balancer_arn = aws_lb.eci_boomi_gateway_lb.arn
  port              = "8077"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.eci_boomi_gateway_tg_8077.arn
  }
}

resource "aws_lb_listener" "eci_boomi_gateway_lb_listener_18077" {
  load_balancer_arn = aws_lb.eci_boomi_gateway_lb.arn
  port              = "18077"
  protocol          = "TCP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.eci_boomi_gateway_tg_18077.arn
  }
}

resource "aws_lb_target_group" "eci_boomi_gateway_tg_8077" {
  name        = "${local.environment}-GATEWAY-TG-8077"
  port        = 8077
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = local.vpc_id
  health_check {
    port                = 8077
    protocol            = "TCP"
  }
}

resource "aws_lb_target_group" "eci_boomi_gateway_tg_18077" {
  name        = "${local.environment}-GATEWAY-TG-18077"
  port        = 18077
  protocol    = "TCP"
  target_type = "instance"
  vpc_id      = local.vpc_id
  health_check {
    port                = 18077
    protocol            = "TCP"
  }
}
