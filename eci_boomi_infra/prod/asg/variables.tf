variable "region" {
  default = "us-east-1"
}

variable "ami" {
  default = "ami-083654bd07b5da81d"
}

variable "instance_type" {
  default = "c5.xlarge"
}

variable "vpc_id" {
  default = "vpc-09e97f76592bdbf6c"
}

variable "ssh_key_name"{
  default  = "ECI_BOOMI_PROD"
}

variable "service_name"{
  default  = "ECI_BOOMI_PROD_GATEWAY"
}

variable "ssl_arn"{
  default  = "arn:aws:acm:us-east-1:327792311537:certificate/cddecbf7-f9a4-4b92-9d10-b9e4e245e841"
}

variable "dns_zone_id"{
  default  = "Z07640302BAWHCZ8KPQYS"
}
