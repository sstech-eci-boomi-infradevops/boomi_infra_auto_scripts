provider "aws" {
  region  = "us-east-1"
}

terraform {
  backend "s3" {
      bucket = "sstech-prod-terraform"
      key = "prod/sstech-gateway.tfstate"
      region = "us-east-1"
    }
}

resource "aws_launch_configuration" "eci_boomi_gateway_lc" {
  name_prefix          = "${local.service_name}_LC_"
  image_id        = local.ami_id
  instance_type   = local.instance_type
  security_groups = ["${aws_security_group.eci_boomi_gateway_sg.id}"]
  key_name        = local.ssh_key_name
  user_data = <<-EOF
    #!/bin/bash
    apt-get update
    apt-get -y install nfs-common
    mkdir /mnt/boomi
    mkdir -p /opt/boomi/local
    sh -c 'echo "fs-070f6807ba6bc9d51.efs.us-east-1.amazonaws.com:/ /mnt/boomi nfs4 defaults,vers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0" >> /etc/fstab'
    mount -a
    useradd -u 1001 boomi
    chown -R boomi:boomi /mnt/boomi/Gateway_ECI_BOOMI_PROD_GATEWAY /opt/boomi
    su boomi -c "/mnt/boomi/Gateway_ECI_BOOMI_PROD_GATEWAY/bin/atom start"
  EOF
  lifecycle {
    create_before_destroy = true
  }
  root_block_device {
    volume_type = local.disk_type
    volume_size = local.disk_size
  }
}

resource "aws_security_group" "eci_boomi_gateway_sg" {
  name   = "eci_boomi_gateway_sg"
  vpc_id = local.vpc_id
}

resource "aws_security_group_rule" "inbound_gateway_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.eci_boomi_gateway_sg.id}"
  to_port           = 65535
  type              = "ingress"
  cidr_blocks       = ["${local.vpc_cidr}"]
}

resource "aws_security_group_rule" "outbound_all" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = "${aws_security_group.eci_boomi_gateway_sg.id}"
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_autoscaling_group" "eci_boomi_gateway_asg" {
  name_prefix                 = "${local.service_name}_ASG_"
  launch_configuration = "${aws_launch_configuration.eci_boomi_gateway_lc.name}"
  vpc_zone_identifier  = local.private_subnets
  max_size                  = local.max_capacity
  min_size                  = local.min_capacity
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = local.desired_capacity
  force_delete              = true
  target_group_arns         = ["${aws_lb_target_group.eci_boomi_gateway_tg_8077.arn}","${aws_lb_target_group.eci_boomi_gateway_tg_18077.arn}"]
  instance_refresh {
    strategy = "Rolling"
    preferences {
      min_healthy_percentage = 50
    }
    triggers = ["tag"]
  }
  tag {
    key                 = "Name"
    value               = local.service_name
    propagate_at_launch = true
  }
}
