locals{
  vpc_id = "vpc-0a460f49de0840542"
  vpc_cidr = "172.20.0.0/16"
  region = "us-east-1"
  environment = "PROD"
  private_subnets = ["subnet-043b6b2760b2f87c5", "subnet-01b418a1c9a7d2bcf", "subnet-02755cef6941b471e"]
  public_subnets = ["subnet-03fbec8b6021f8c63", "subnet-0d4a1aef60f10f238", "subnet-0bd4b6914aafe9646"]
  load_balancer_name = "eci-boomi-prod-gateway"
  load_balancer_type = "network"
  service_name = "ECI_BOOMI_PROD_GATEWAY"
  ami_id = "ami-083654bd07b5da81d"
  desired_capacity = 3
  min_capacity = 3
  max_capacity = 12
  instance_type = "c5.xlarge"
  ssh_key_name = "ECI_BOOMI_PROD"
  disk_size = 50
  disk_type = "gp3"
}
