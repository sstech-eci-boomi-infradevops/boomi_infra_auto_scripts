output "vpc_id" {
  description = "VPC ID"
  value       = module.vpc.vpc_id
}

output "public_subnet_ids" {
  description = "PUBLIC SUBNET ID's"
  value       = module.vpc.public_subnets
}

output "private_subnet_ids" {
  description = "PRIVATE SUBNET ID's"
  value       = module.vpc.private_subnets
}
