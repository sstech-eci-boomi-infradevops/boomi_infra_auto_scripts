provider "aws" {
  region  = "us-east-1"
}

terraform {
  backend "s3" {
    bucket = "sstech-prod-terraform"
    key = "prod/vpc.tfstate"
    region = "us-east-1"
  }
}

locals{
  # EKS Cluster Variables
  eks_cluster_name = "ECI_BOOMI_PROD_EKS"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "ECI_BOOMI_PROD_VPC"
  cidr = "172.20.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["172.20.0.0/19", "172.20.32.0/19", "172.20.64.0/19"]
  public_subnets  = ["172.20.96.0/19", "172.20.128.0/19", "172.20.160.0/19"]

  enable_nat_gateway = true
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway  = false

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }


  tags = {
    Terraform = "true"
    Environment = "PROD"
  }
}
