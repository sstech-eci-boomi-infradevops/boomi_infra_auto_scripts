module "bastion" {
  source = "umotif-public/bastion/aws"
  version = "~> 2.1.0"

  name_prefix = "eci-boomi-prod"

  vpc_id         = module.vpc.vpc_id
  public_subnets = flatten([module.vpc.public_subnets])

  # hosted_zone_id = var.dns_zone_id
  ssh_key_name   = var.bation_key_pair

  enable_asg_scale_down = false
  enable_asg_scale_up   = false

  delete_on_termination = true
  volume_size           = 10
  encrypted             = true

  # userdata_file_content = templatefile("./custom-userdata.sh", {}) # if you want to use default one, simply remove this line

  tags = {
    Name        = "ECI_BOOMI_PROD_BASTION"
    Environment = "PROD"
  }
}
