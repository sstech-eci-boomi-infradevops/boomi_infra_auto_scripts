variable "aws_region" {
  default = "us-east-1"
}

variable "bation_key_pair"{
  default = "ECI_BOOMI_PROD"
}

variable "instance_type" {
  type        = string
  default     = "t2.medium"
  description = "Bastion instance type"
}

variable "user_data" {
  type        = list(string)
  default     = []
  description = "User data content"
}

variable "associate_public_ip_address" {
  type        = bool
  default     = true
  description = "Whether to associate public IP to the bastion instance."
}

variable "dns_zone_id" {
  default = "Z04483651A5GJ9V38TQZ3"
}
