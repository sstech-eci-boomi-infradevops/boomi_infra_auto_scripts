module "efs_controller" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.6.0"
  create_role                   = true
  role_name                     = "efs-controller-${local.cluster_name}"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.efs_controller.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:kube-system:efs-controller"]
}

resource "aws_iam_policy" "efs_controller" {
  name_prefix = "efs-controller-${local.cluster_name}"
  description = "EKS efs-controller policy for cluster ${module.eks.cluster_id}"
  policy      = file("iam/efs-iam-policy.json")
}
