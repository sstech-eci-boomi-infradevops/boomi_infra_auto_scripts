provider "aws" {
  region  = "us-east-1"
}

terraform {
  backend "s3" {
      bucket = "sstech-prod-terraform"
      key = "prod/eks.tfstate"
      region = "us-east-1"
    }
}

data "aws_eks_cluster" "cluster" {
  name = module.eks.cluster_id
}

data "aws_eks_cluster_auth" "cluster" {
  name = module.eks.cluster_id
}

data "aws_availability_zones" "available" {}

data "aws_caller_identity" "current" {}

provider "kubernetes" {
  host                   = data.aws_eks_cluster.cluster.endpoint
  cluster_ca_certificate = base64decode(data.aws_eks_cluster.cluster.certificate_authority[0].data)
  token                  = data.aws_eks_cluster_auth.cluster.token
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  version         = "17.20.0"
  cluster_name    = local.cluster_name
  cluster_version = local.cluster_version
  subnets         = local.subnets
  vpc_id          = local.vpc_id
  write_kubeconfig = false
  enable_irsa     = true
  cluster_endpoint_private_access   = true
  cluster_endpoint_public_access    = true
  # cluster_endpoint_public_access_cidrs = local.public_access_cidrs
  cluster_enabled_log_types = local.cluster_enabled_log_types
  # manage_aws_auth = false
  tags = {
    Environment = "${local.environment}"
  }

  node_groups_defaults = {
    ami_type  = local.ami_type
    disk_size = local.disk_size
    disk_type = local.disk_type
  }

  node_groups = {
    ng1 = {
      name_prefix  = "pac-ng01-"
      desired_capacity = local.desired_capacity
      max_capacity     = local.max_capacity
      min_capacity     = local.min_capacity
      instance_types = local.instance_types
      capacity_type  = local.capacity_type
      source_security_group_ids = [aws_security_group.worker_mgmt.id]
      # node_role_arn    = local.node_role_arn
      key_name = local.ssh_key_name
      tags = [
        {
          "key"                 = "Name"
          "propagate_at_launch" = "true"
          "value"               = "ECI_BOOMI_PROD_EKS_WORKER"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/enabled"
          "propagate_at_launch" = "false"
          "value"               = "true"
        },
        {
          "key"                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          "propagate_at_launch" = "false"
          "value"               = "owned"
        },
      ]
      k8s_labels = {
        Environment = "${local.environment}"
      }

    }
  }
  map_roles  = var.map_roles
  map_users  = var.map_users
}

resource "aws_security_group" "worker_mgmt" {
  name_prefix = "EKS_NG_MGMNT"
  vpc_id      = local.vpc_id

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = [
      "172.20.0.0/16",
    ]
  }
}
