module "cert-manager" {
  source                        = "terraform-aws-modules/iam/aws//modules/iam-assumable-role-with-oidc"
  version                       = "4.6.0"
  create_role                   = true
  role_name                     = "cert-manager-${local.cluster_name}"
  provider_url                  = replace(module.eks.cluster_oidc_issuer_url, "https://", "")
  role_policy_arns              = [aws_iam_policy.cert-manager.arn]
  oidc_fully_qualified_subjects = ["system:serviceaccount:cert-manager:cert-manager"]
}

resource "aws_iam_policy" "cert-manager" {
  name_prefix = "cert-manager-${local.cluster_name}"
  description = "EKS cert-manager policy for cluster ${module.eks.cluster_id}"
  policy      = file("iam/cert-manager-policy.json")
}
