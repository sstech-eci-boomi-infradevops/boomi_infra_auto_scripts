locals{
  # EKS Cluster Variables
  cluster_name = "ECI_BOOMI_PROD_EKS"
  vpc_id = "vpc-0a460f49de0840542"
  region = "us-east-1"
  subnets = ["subnet-043b6b2760b2f87c5", "subnet-01b418a1c9a7d2bcf", "subnet-02755cef6941b471e"]
  cluster_version = "1.19"
  environment = "PROD"
  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  # Managed nodeGroups Variables
  desired_capacity = 3
  min_capacity = 3
  max_capacity = 12
  instance_types = ["m5.xlarge"]
  capacity_type = "ON_DEMAND"
  ssh_key_name = "ECI_BOOMI_PROD"
  ami_type = "AL2_x86_64"
  disk_size = 50
  disk_type = "gp3"
}
