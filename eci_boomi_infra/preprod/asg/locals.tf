locals{
  vpc_id = "vpc-09e97f76592bdbf6c"
  vpc_cidr = "172.19.0.0/16"
  region = "us-west-2"
  environment = "PREPROD"
  private_subnets = ["subnet-04bd4e5e6cbcda1f7", "subnet-027d5700927200875", "subnet-01d143ad2a08b9358"]
  public_subnets = ["subnet-04bd4e5e6cbcda1f7", "subnet-027d5700927200875", "subnet-01d143ad2a08b9358"]
  load_balancer_name = "eci-boomi-preprod-gateway"
  load_balancer_type = "network"
  service_name = "ECI_BOOMI_PREPROD_GATEWAY"
  ami_id = "ami-0a32b4de33a6c1bbd"
  desired_capacity = 3
  min_capacity = 3
  max_capacity = 12
  instance_type = "c5.xlarge"
  ssh_key_name = "ECI_BOOMI_PREPROD"
  disk_size = 50
  disk_type = "gp3"
}
