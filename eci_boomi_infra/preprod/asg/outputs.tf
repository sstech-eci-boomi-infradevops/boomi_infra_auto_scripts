output "aws_lb" {
  description = "ELB DNS"
  value       = aws_lb.eci_boomi_gateway_lb.dns_name
}
