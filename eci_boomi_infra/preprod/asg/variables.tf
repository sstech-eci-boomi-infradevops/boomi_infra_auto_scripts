variable "region" {
  default = "us-west-2"
}

variable "ami" {
  default = "ami-0a32b4de33a6c1bbd"
}

variable "instance_type" {
  default = "c5.xlarge"
}

variable "vpc_id" {
  default = "vpc-09e97f76592bdbf6c"
}

variable "ssh_key_name"{
  default  = "ECI_BOOMI_PREPROD"
}

variable "service_name"{
  default  = "ECI_BOOMI_PREPROD_GATEWAY"
}

variable "ssl_arn"{
  default  = "arn:aws:acm:us-east-1:327792311537:certificate/cddecbf7-f9a4-4b92-9d10-b9e4e245e841"
}

variable "dns_zone_id"{
  default  = "Z0318525377P4JNWOT5V7"
}
