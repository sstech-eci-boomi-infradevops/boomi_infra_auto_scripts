provider "aws" {
  region  = "us-west-2"
}

terraform {
  backend "s3" {
    bucket = "sstech-terraform"
    key = "preprod/vpc.tfstate"
    region = "us-west-2"
  }
}

locals{
  # EKS Cluster Variables
  eks_cluster_name = "ECI_BOOMI_PREPROD_PAC_EKS"
}


module "vpc" {
  source = "terraform-aws-modules/vpc/aws"
  version = "3.7.0"

  name = "ECI_BOOMI_PREPROD_VPC"
  cidr = "172.19.0.0/16"

  azs             = ["us-west-2a", "us-west-2b", "us-west-2c"]
  private_subnets = ["172.19.1.0/24", "172.19.2.0/24", "172.19.3.0/24"]
  public_subnets  = ["172.19.101.0/24", "172.19.102.0/24", "172.19.103.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true
  enable_dns_hostnames = true
  single_nat_gateway  = true

  public_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/elb"              = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${local.eks_cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"     = "1"
  }


  tags = {
    Terraform = "true"
    Environment = "PREPROD"
  }
}
