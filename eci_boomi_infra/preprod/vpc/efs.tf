module "efs" {
  source = "cloudposse/efs/aws"
  # Cloud Posse recommends pinning every module to a specific version
  version     = "0.31.1"

  namespace       = "ECI-BOOMI"
  stage           = "PREPROD"
  name            = "EFS"
  region          = "us-west-2"
  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.private_subnets
  # throughput_mode = provisioned
  # provisioned_throughput_in_mibps =
  # zone_id         = var.dns_zone_id
  security_group_rules = [
  {
    type                     = "egress"
    from_port                = 0
    to_port                  = 65535
    protocol                 = "-1"
    cidr_blocks              = ["0.0.0.0/0"]
    source_security_group_id = null
    description              = "Allow all egress trafic"
  },
  {
    type                     = "ingress"
    from_port                = 2049
    to_port                  = 2049
    protocol                 = "tcp"
    cidr_blocks              = ["172.19.0.0/16"]
    source_security_group_id = null
    description              = "Allow ingress traffic to EFS from trusted Security Groups"
  }
]
}
