locals{
  # EKS Cluster Variables
  cluster_name = "ECI_BOOMI_PREPROD_PAC_EKS"
  vpc_id = "vpc-09e97f76592bdbf6c"
  region = "us-west-2"
  subnets = ["subnet-04bd4e5e6cbcda1f7", "subnet-027d5700927200875", "subnet-01d143ad2a08b9358"]
  cluster_version = "1.19"
  environment = "PREPROD"
  cluster_enabled_log_types = ["api", "audit", "authenticator", "controllerManager", "scheduler"]

  # Managed nodeGroups Variables
  desired_capacity = 3
  min_capacity = 3
  max_capacity = 12
  instance_types = ["m5.xlarge"]
  capacity_type = "ON_DEMAND"
  ssh_key_name = "ECI_BOOMI_PREPROD"
  ami_type = "AL2_x86_64"
  disk_size = 50
  disk_type = "gp3"
}
