variable "region" {
  default = "ue-west-2"
}

variable "cluster_name" {
  default = "ECI_BOOMI_PREPROD_PAC_EKS"
}

variable "map_roles" {
  description = "Additional IAM roles to add to the aws-auth configmap."
  type = list(object({
    rolearn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      rolearn  = "arn:aws:iam::646455780665:group/Admin"
      username = "{{SessionName}}"
      groups   = ["system:masters"]
    },
  ]
}

variable "map_users" {
  description = "Additional IAM users to add to the aws-auth configmap."
  type = list(object({
    userarn  = string
    username = string
    groups   = list(string)
  }))

  default = [
    {
      userarn  = "arn:aws:iam::646455780665:user/rajesh804@gmail.com"
      username = "Rajesh.Reddy@sstech.us"
      groups   = ["system:masters"]
    },
  ]
}
